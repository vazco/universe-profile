'use strict';

UniProfile.addRoute('uniProfile', {
    path: '/profile/:_id',
    waitOn: function () {
        return [
            Meteor.subscribe('uniProfileUser', this.params._id),
            Meteor.subscribe('profileAvatar')
        ];
    },
    data: function () {
        return {
            user: UniUsers.findOne(this.params._id)
        }
    }
});