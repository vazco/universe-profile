'use strict';

Meteor.startup(function () {
    Meteor.call('checkPublications', function (err) {
        if (err) {
            throw err;
        }
    });
    if(UniProfile.autoSubscribeAvatars){
        var avatars_subs = {};
        UniUsers.find().observeChanges({
            added: function(id){
                if(avatars_subs[id]){
                    avatars_subs[id].stop();
                }
                avatars_subs[id] = Meteor.subscribe('userAvatar', id);
            },
            removed: function(id){
                if(avatars_subs[id]){
                    avatars_subs[id].stop();
                }
            }
        });
    }
});


var _fieldHelpers = {
    getPlaceholder: function () {
        var parentData = Template.parentData(1);
        if(typeof this === 'string'){
            return typeof i18n !== 'undefined' && i18n(this) || this;
        }else if(typeof parentData === 'string'){
            return typeof i18n !== 'undefined' && i18n(parentData) || parentData;
        }
        return '';
    },
    getClass: function () {
        return 'profile-field-'+this.replace('.', '_');
    },
    user: function () {
        for(var i=0; i<10; i++){
            if(Template.parentData(i) && Template.parentData(i).user){
                return Template.parentData(i).user;
            }
        }

        var user_id = Router.current().params._id;
        return Meteor.users.findOne(user_id);
    }
};

UniProfile.addHelpers('uniProfileField', _fieldHelpers);
UniProfile.addHelpers('uniProfileFieldWithLabel', _fieldHelpers);
UniProfile.addHelpers('uniProfileFieldEdit', _fieldHelpers);
UniProfile.addHelpers('uniProfileLabel', _fieldHelpers);

UniProfile.addHelpers('uniProfileFormFields', {
    omitProfileFields: function () {
        return UniProfile.omitProfileFields;
    },
    omitUserFields: function () {
        return UniProfile.omitUserFields;
    }
});

UniProfile.addHelpers('uniProfileFieldWithLabel', {
    isSetVal: function (user, name){
        return Vazco.get(user, name) !== undefined;
    }
});


/**
 * Remove all avatars where owner is current logged in user
 * @private
 */
var _removeUserAvatar = function () {
    var existingAvatars = ProfileAvatar.find({
        'metadata.owner': Meteor.userId()
    });

    existingAvatars.forEach(function (avatar) {
        avatar.remove();
    });
};

var _addFileEvent = function (e,ui) {
    var _id = ui.data._id,
        _logged = Meteor.user();

    if(typeof(_id) === 'undefined'){
        _id = Meteor.userId();
    }

    if(_logged._id === _id || _logged.is_admin){

        _removeUserAvatar();

        // MZ: temporary fix (VazcoFilesUpload.insertFile uses window.event
        // wchich is not available in Firexox, Opera, ...)
        window.event = e;

        VazcoFilesUpload.insertFile(ProfileAvatar, 'myForm', {
            metadata: {
                owner: _id
            },
            after: function (error, fileObj) {
                if(error){
                    console.error(error);
                    return;
                }
                console.log('Inserted file name:', fileObj.name());
            }
        });
    }
};

UniProfile.addEvents('uniProfileAvatar', {
    'dropped .vfu-dropzone-files': _addFileEvent,
    'change .vfu-hidden-input-files': _addFileEvent
});

UniProfile.addHelpers('uniProfileAvatar', {
    profileAvatar: function () {
        return ProfileAvatar.find({'metadata.owner':this._id});
    }
});

Template.uniUserAvatar.created = function(){
    this.view._subscriptionForUserAvatar =
        Meteor.subscribe('userAvatar', Vazco.get(this, 'data.user._id') || Vazco.get(this, 'data._id'));
};

Template.uniUserAvatar.destroyed = function(){
    if(this.view._subscriptionForUserAvatar){
        this.view._subscriptionForUserAvatar.stop();
    }
};

UniProfile.addHelpers('uniUserAvatar', {
    userAvatar: function () {
        var user = Vazco.get(this, 'user') || this;
        var className = Vazco.get(this, 'className') || '';
        var classModifier = Vazco.get(this, 'classModifier');
        className += classModifier?((className?' ':'')+'uni-user-avatar--'+classModifier):'';
        if(!className){
            className = 'user-regular-thumbs';
        }
        if(!Vazco.has(user, 'profile')){
            console.warn('Missing user document!');
        }
        return {
            className: className,
            icon: ProfileAvatar.findOne({'metadata.owner':user._id})
        };
    }
});

UniProfile.addHelpers('uniProfileButtons', {
    pluginChatExist: function () {
        return UniChat && UniChat.name === 'UniChat';
    }
});