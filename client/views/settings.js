'use strict';

(Meteor.isClient ? window : global).changePassForm = new SimpleSchema({
    password: {
        type: String,
        label: 'New password',
        optional: true
    },
    passwordrepeat: {
        type: String,
        label: 'Confirm Password',
        custom: function() {
            if (this.value !== this.field('password').value) {
                return 'passwordMismatch';
            }
        }
    },
    passwordold: {
        type: String,
        label: 'Old password'
    }
});

(Meteor.isClient ? window : global).addEmailForm = new SimpleSchema({
    email: {
        type: String,
        label: 'New email'
    }
});

changePassForm.messages({
    'passwordMismatch': 'Passwords do not match',
    'regEx email': '[label] is not a valid e-mail address',
    'notCorrectEmail': 'Please fill your email address'
});



AutoForm.addHooks('changePassForm', {
    onSubmit: function(insertDoc) {
        Accounts.changePassword(insertDoc.passwordold, insertDoc.password, function(error) {
            if (error) {
                Vazco.setErrorMessage('profile_settings', error.reason);
                $('.btn-change-pass').prop('disabled', false);
            } else {
                Vazco.setSuccessMessage('profile_settings', i18n('registration.passChangeOk'));
                $('.btn-change-pass').prop('disabled', false);
                $('#changePassForm')[0].reset();
            }
        });
        return false;
    },
    onError: function(e) {
        console.log(e, arguments);
    }
});

AutoForm.addHooks('addEmailForm', {
    onSubmit: function(insertDoc) {
        var _emailAddr = insertDoc.email,
            _user = Meteor.userId(),
            _existAddressCheck = UniUsers.findOne({emails:{$elemMatch: {address:_emailAddr}}});

        if(typeof(_existAddressCheck) === 'undefined'){
            Meteor.call('changeUserEmail',_user,_emailAddr, function (err) {
                if(err){
                    Vazco.setErrorMessage('profile_settings', err);
                }else{
                    Vazco.setSuccessMessage('profile_settings', 'Address added! Check Your mailbox for verification.');
                }
            });
        }else{
            Vazco.setErrorMessage('profile_settings', 'Address exists!');
        }

        $('#addEmailForm')[0].reset();
        $('.btn-change-email').prop('disabled', false);
        return false;
    },
    onError: function(e) {
        console.log(e, arguments);
    }
});


Template.uniProfileSettingsEmail.helpers({
    getOtherEmails: function () {

        return _.without(this.emails, this.emails[0]);
    }
});


Template.uniProfileSettingsEmail.events({
    'click .other-emails .email-set-default': function (e) {
        var el = $(e.target);
        var object_id = el.data('id');
        var self = this;

        var user = UniUsers.findOne(object_id, {fields: {emails: 1}});
        var emails = user.emails;
        var new_emails = [];

        new_emails.push(self);
        _.each(emails, function (email) {
            if(email.address !== self.address){
                new_emails.push(email);
            }
        });

        UniUsers.update(object_id, {$set: {emails: new_emails}});
    },
    'click .other-emails .email-remove': function (e) {
        var el = $(e.target);
        var object_id = el.data('id');
        var self = this;

        var user = UniUsers.findOne(object_id, {fields: {emails: 1}});
        var emails = user.emails;
        var remove_email;

        _.each(emails, function (email) {
            if(email.address === self.address){
                remove_email = email;
            }
        });

        UniUsers.update(object_id, {$pull: {emails: remove_email}});
    }
});
