'use strict';

var friendHelpers = {
    isMyFriend: function() {
        var friend_id = this._id;

        if (!Meteor.user() || !friend_id) {
            return false;
        }

        if (_.indexOf(Meteor.user().friends, friend_id) !== -1) {
            return true;
        }

        return false;
    },
    requestSend: function() {
        var friend = Meteor.users.findOne(this._id, {friends: true});

        if (!Meteor.user() || !friend) {
            return false;
        }

        if (_.indexOf(friend.friends_requests, Meteor.userId()) !== -1) {
            return true;
        }

        return false;
    },
    requestReceived: function() {
        var friend_id = this._id;
        var user = Meteor.users.findOne(Meteor.userId(), {friends_requests: true});

        if (!user || !friend_id) {
            return false;
        }

        if (_.indexOf(Meteor.user().friends_requests, friend_id) !== -1) {
            return true;
        }

        return false;
    },
    ownProfile: function(id) {
        if (!Meteor.user()) {
            return false;
        }
        if (id === Meteor.user()._id) {
            return true;
        }
        return false;
    }
};

Template.userAddFriend.helpers(friendHelpers);
Template.userAddFriendSimple.helpers(friendHelpers);
Template.userAddFriendMenuInProfile.helpers(friendHelpers);

Template.friendsRequestsListingMenu.requestsList = function () {
    var user = Meteor.user();
    if (user) {
        var friends_requests = user.friends_requests;
        var requests_users = [];

        if (!friends_requests) {
            return false;
        }

        _.each(friends_requests, function (id) {
            var user = Meteor.users.findOne(id);
            requests_users.push(user);
        });


        return requests_users;
    }
};

Template.friendsRequestsCount.friendsRequestsCount = function () {
    var user = Meteor.user();
    if (user && user.friends_requests) {
        return user.friends_requests.length;
    }
};

var friendEvents = {
    'click .friend-request-send': function() {
        var friend_id = this._id;

        Meteor.call('friends/sendRequest', friend_id);
    },
    'click .friend-request-cancel': function() {
        var friend_id = this._id;

        Meteor.call('friends/cancelRequest', friend_id);
    },
    'click .friend-remove': function() {
        var friend_id = this._id;

        Meteor.call('friends/removeFriend', friend_id);
    },
    'click .friend-request-accept': function() {
        var friend_id = this._id;

        Meteor.call('friends/acceptRequest', friend_id);
    },
    'click .friend-request-reject': function() {
        var friend_id = this._id;

        Meteor.call('friends/rejectRequest', friend_id);
    }
};

Template.userAddFriend.events(friendEvents);
Template.userAddFriendSimple.events(friendEvents);
Template.userAddFriendMenuInProfile.events(friendEvents);

var clickOpenChat = {
    'click .message-open-chat': function() {
        var recipient_id = this._id;
        var user_id = Meteor.userId();

        if (!user_id || !recipient_id) {
            throw new Meteor.Error(404, 'No such user_id or recipient_id');
        }

        var recipients = [recipient_id, user_id];

        var chatroom_id = Chatrooms.openChatroomWithUsers(recipients);

        if (chatroom_id) {
            Chatrooms.showChatroom(chatroom_id);
        }
    }
};
Template.friendsOnlineListing.events(clickOpenChat);
Template.friendsOnlineListing.friendsOnlineList = function () {
    if (Meteor.userId()) {
        return Meteor.users.find({friends :{
            $in : [Meteor.userId()]
        }});
    }
};
Template.friendsOnlineListing.rendered = function() {
//    $('.friendsOnlineListing').lionbars(false);
};

UI.registerHelper('getFriends', function (userId) {
    var user;
    if(userId){
        user = Meteor.users.findOne(userId);
    }else {
        user = Meteor.user();
    }

    if(user){
        var friends = UniUtils.get(user,'friends',false);
        if(_.isArray(friends)){
            return Meteor.users.find({_id: {$in:friends}});
        }
    }else{
        return [];
    }
});