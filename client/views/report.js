'use strict';

Template.uniProfileReport.events({
    'click .user-profile-raport-send': function(e) {
        var reason = $(e.target).parent().parent().find('textarea').val();


        var data = {
            data: {
                user_id: this._id,
                reason: reason
            }
        };

        Meteor.call('addUserReport', data, function() {
            $('#reportModal').modal('toggle');
        });
    }
});