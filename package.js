Package.describe({
    name: 'vazco:universe-profile',
    summary: 'User profile',
    version: '0.9.6'
});

Package.on_use(function (api) {
    api.use([
        'iron:router',
        'vazco:universe-core',
        'vazco:universe-core-plugin',
        'anti:i18n',
        'aldeed:simple-schema',
        'aldeed:autoform',
        'vazco:files-upload',
        'vazco:tools-common',
        'vazco:universe-html-purifier@1.1.0'
    ], ['client', 'server']);

    api.use('templating', 'client');
    api.add_files(['plugin.js']);

    api.add_files([
        'lib/collections/Profile.js',
        'lib/collections/Avatar.js',
        'lib/localization/localization.js'
    ], ['client', 'server']);

    api.add_files([
        'client/views/router.js',
        'client/views/profile.html',
        'client/views/profile.js',
        'client/stylesheet/profile.css',
        'client/views/report.html',
        'client/views/report.js',
        'client/views/settings.html',
        'client/views/settings.js',
        'client/views/friends.html',
        'client/views/friends.js'
    ], 'client');

    api.add_files([
        'server/publications.js',
        'server/methods.js'
    ], 'server');

    api.export('UniProfile');
});
