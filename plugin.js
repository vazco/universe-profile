UniProfile = new UniPlugin('UniProfile');

UniProfile.autoSubscribeAvatars = true;

UniProfile.config = function (config) {
    if(!config){
        throw new Meteor.Error( 500, 'config can not be empty. ' +
            'Available options: "profileSchema", "userSchema", "omitUserFields", "omitProfileFields"');
    }

    // profile and user schema

    this.profileSchema = _.extend(this.profileSchema, config.profileSchema || {});

    this.userSchema.profile = {
        type: new SimpleSchema(this.profileSchema),
        optional: true
    };

    this.userSchema = _.extend(this.userSchema, config.userSchema || {});

    UniUsers.attachSchema(new SimpleSchema(this.userSchema), {replace: true});


    // Omit fields

    if(config.omitUserFields) {
        this.omitUserFields = config.omitUserFields;
    }

    if(config.omitProfileFields){
        this.omitProfileFields = config.omitProfileFields;
    }

    this.autoSubscribeAvatars = UniUtils.get(config, 'autoSubscribeAvatars', UniProfile.autoSubscribeAvatars, true);
};
