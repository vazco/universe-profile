# User Profile

> This package is part of Universe, a framework based on [Meteor platform](http://meteor.com)
maintained by [Vazco](http://www.vazco.eu).

> It works standalone, but you can get max out of it when using the whole system.

## Usage

You need add in template link to profile

```
#!html

<a href="{{pathFor 'uniProfile' _id=currentUser._id}}">Profile</a>
```

## Options

This package sets the default profile schema with typical fields (profile.name, profile.surname, profile.about, profile.address, profile.zip, profile.country, profile.phone, profile.mobile).

#### 1. If you would like add other fields to "user.profile." you should add this field to: "profileSchema":

```
#!javascript
UniProfile.config({
  profileSchema: {
    test1: {
      type: String,
      label: i18n("profile.test3Label"),
      optional: true
    },
    test2: {
      type: String,
      label: i18n("profile.test4Label"),
      optional: true,
      autoform: {
      type: "contenteditable"
    }
  }
});
```

#### 2. If you would like add fields to "user." you should add this field to: "userSchema":

```
#!javascript
UniProfile.config({
  userSchema: {
    test3: {
      type: String,
      label: i18n("profile.test3Label"),
      optional: true
    },
    test4: {
      type: String,
      label: i18n("profile.test4Label"),
      optional: true,
      autoform: {
      type: "contenteditable"
    }
  }
});
```

#### 3. All fields (in user and user.profile) are displayed in profile. If you would like to not display some fields you can hide these:

```
#!javascript
UniProfile.config({
  omitUserFields: 'test4, _id, emails, createdAt, is_admin, services, permissions, disabled, profile'
  omitProfileFields: 'profile.test1, profile.name, profile.surname'
});
```

Some fields is ommited default by plugin (_id, emails, createdAt, is_admin, services, permissions, disabled, profile).


#### Example config

```
#!javascript

UniProfile.config({
    userSchema: {
        test1: {
            type: String,
            label: i18n("profile.test1Label"),
            optional: true
        },
        test2: {
            type: String,
            label: i18n("profile.test2Label"),
            optional: true,
            autoform: {
                type: "contenteditable"
            }
        }
    },
    profileSchema: {
        test3: {
            type: String,
                label: i18n("profile.test3Label"),
                optional: true
        },
        test4: {
            type: String,
            label: i18n("profile.test4Label"),
            optional: true,
            autoform: {
                type: "contenteditable"
            }
        }
    },
    omitUserFields: 'test2, _id, emails, createdAt, is_admin, services, permissions, disabled, profile',
    omitProfileFields: 'profile.test4, profile.name, profile.surname'
});
```

## Publications

You need to create publications like this:

```
#!javascript
Meteor.publish('uniProfileUser', function(id) {
    return Meteor.users.find(id, {fields: { createdAt:1, profile:1, emails: 1, status:1, is_admin:1 }});
});

Meteor.publish('userAvatar', function (_id){
    return ProfileAvatar.find({'metadata.owner':_id});
});
```
### Automatic subscriptions of avatars
UniProfile subscribes an avatar for each user.
To do that, it use subscription named 'userAvatar' for each available user.
To disable this functionality, you must set 'autoSubscribeAvatars' in UniProfile.config as a false:
UniProfile.config({autoSubscribeAvatars: false})

## Access

You need create access like this example:

```
#!javascript
UniUsers.allow({
    update: function (userId, doc, fieldNames) {
        if (userId && doc._id === userId && _.indexOf(fieldNames, 'is_admin') === -1) {
            return true;
        }

        // admin can modify any
        return (Meteor.userId() && Meteor.user().is_admin);
    }
});

ProfileAvatar.allow({
    insert: function () {
        return true;
    },
    update: function (userId, entity) {
        return entity.metadata.owner === userId;
    },
    download: function () {
        return true;
    },
    remove: function (userId, entity) {
        return entity.metadata.owner === userId;
    }
});
```


## Friends

In top menu:

```
#!html

{{> friendsRequestsListingMenu}}

{{> friendsRequestsCount}}
```