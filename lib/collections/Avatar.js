'use strict';

(Meteor.isClient ? window : global).ProfileAvatar = VazcoFilesUpload.init({
    name: 'ProfileAvatar',
    //stores: ['files', 'thumbs'],
    maxFileSize: 50000000,
    contentTypes: ['image/*'],
    extensions: ['jpg', 'jpeg', 'png', 'gif'],
    thumbnailSize: ['160', '160']
});
