"use strict";


//schema for specific user fields

UniProfile.profileSchema = {
    name: {
        type: String,
        label: i18n("profile.nameLabel"),
        autoValue: function(){
            if(this.value){
                return this.value.replace(/<(?:.|\n)*?>/gm, '');
            }
        }
        //autoform: {
        //    type: "contenteditable"
        //}
    },
    surname: {
        type: String,
        label: i18n("profile.surnameLabel"),
        autoValue: function(){
            if(this.value){
                return this.value.replace(/<(?:.|\n)*?>/gm, '');
            }
        },
        optional: true
    },
    about: {
        type: String,
        label: i18n("profile.aboutLabel"),
        autoValue: function(){
            if(this.value){
                return UniHTML.purify(this.value, {catchErrors: true});
            }
        },
        optional: true
    },
    address: {
        type: String,
        label: i18n("profile.addressLabel"),
        autoValue: function(){
            if(this.value){
                return this.value.replace(/<(?:.|\n)*?>/gm, '');
            }
        },
        optional: true
    },
    zip: {
        type: String,
        label: i18n("profile.zipLabel"),
        autoValue: function(){
            if(this.value){
                return this.value.replace(/<(?:.|\n)*?>/gm, '');
            }
        },
        optional: true
    },
    country: {
        type: String,
        label: i18n("profile.countryLabel"),
        autoValue: function(){
            if(this.value){
                return this.value.replace(/<(?:.|\n)*?>/gm, '');
            }
        },
        optional: true
    },
    phone: {
        type: String,
        label: i18n("profile.phoneLabel"),
        autoValue: function(){
            if(this.value){
                return this.value.replace(/<(?:.|\n)*?>/gm, '');
            }
        },
        optional: true
    },
    mobile: {
        type: String,
        label: i18n("profile.mobileLabel"),
        autoValue: function(){
            if(this.value){
                return this.value.replace(/<(?:.|\n)*?>/gm, '');
            }
        },
        optional: true
    }
};

UniProfile.userSchema = {
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id
    },
    username: {
        type: String,
        //        regEx: /^[a-z0-9A-Z_]{3,15}$/,
        label: i18n('profile.username')
    },
    emails: {
        type: [Object]
    },
    'emails.$.address': {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    'emails.$.verified': {
        type: Boolean,
        optional: true
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: new SimpleSchema(UniProfile.profileSchema),
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    is_admin: {
        type: Boolean,
        optional: true
    },
    permissions: {
        type: Object,
        blackbox: true,
        optional: true
    },
    disabled: {
        type: Boolean,
        optional: true,
        defaultValue: false
    }
};

UniUsers.attachSchema(UniProfile.userSchema);


//default omit fields
UniProfile.omitProfileFields = 'profile.name, profile.surname, profile.about';
UniProfile.omitUserFields = '_id, username, emails, createdAt, is_admin, services, permissions, disabled, profile';


UniUsers.helpers({
    email: function () {
        var user = UniUsers.findOne(this._id);

        if (user && typeof(user.emails) !== "undefined" && user.emails[0] && user.emails[0].address) {
            return user.emails[0].address;
        }
    },

    /**
     * Get user icon URL
     */
    iconUrl: function (filesStore) {
        var avatar = ProfileAvatar.findOne({
            "metadata.owner": this._id
        });

        return avatar.url(filesStore);
    }
});
