'use strict';

i18n.setLanguage('en');

i18n.map('en', {
    profile: {
        title:  'Profile',
        clickToEdit: 'Click for edit',
        name:'Name',
        surname:'Surname',
        about: 'About me',
        expertise: 'Expertise',
        email:'Other E-mails',
        defaultEmail: 'Default E-mail',
        phone:'Phone',
        mobile:'Mobile',
        address:'Street, Address',
        zip:'ZIP, Town',
        country:'Country',
        buttonReportUser: 'Report user',
        userOnline: 'User online',
        userOffline: 'User offline',
        uploadAvatar: 'Upload avatar',
        lastLogin: 'last login',
        username: 'Username',
        report: {
            title: 'Report user',
            reason: 'Reason',
            buttonClose: 'Close',
            send: 'Send'
        },
        settings: {
            title: 'Settings',
            passChange: 'Change password',
            close: 'Close',
            email: 'Email',
            passwordNew: 'New password',
            confirmNewPassword: 'Repeat new password',
            passwordOld: 'Old password',
            defaultEmailLabel: 'Default Email',
            emailLabel: 'Other Emails',
            addEmail: 'Add E-mail'

        }
    },
    registration:{
        passChangeOk:"Password has been changed successfully"
    }
});