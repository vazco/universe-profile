'use strict';

Meteor.methods({
    changeUserEmail:function(userId, email){
        this.unblock();

        check(userId, String);
        check(email, String);

        email = email.toLowerCase();

        var user = Meteor.user();
        if (!user || user._id !== userId) {
            throw new Meteor.Error(403, 'Access denied');
        }

        UniUsers.update(userId,{$addToSet:{emails:{address:email, verified:false}}});
        return Accounts.sendVerificationEmail(userId, email);
    },
    checkPublications: function () {
        _(['uniProfileUser', 'userAvatar']).each(function (publication_name) {
            if(!Vazco.get(Meteor.server.publish_handlers, publication_name)){
                throw new Meteor.Error(404, 'No such publication "'+publication_name+'". Go to readme.');
            }
        });
    }
});


// friends

Meteor.methods({
    'friends/sendRequest': function(friend_id) {
        this.unblock();

        var friend = Meteor.users.findOne(friend_id);

        if (!Meteor.user()) {
            throw new Meteor.Error(403, 'You need to be logged in, to send message');
        }
        if (!friend) {
            throw new Meteor.Error(404, 'No such friend');
        }

        var friends_requests = friend.friends_requests;

        if (!_.isArray(friends_requests)) {
            friends_requests = [];
        }

        if (_.indexOf(friends_requests, Meteor.userId()) !== -1) {
            return false;
        }

        friends_requests.push(Meteor.userId());

        return Meteor.users.update(friend_id, {$set: {friends_requests: friends_requests}});
    },
    'friends/cancelRequest': function(friend_id) {
        this.unblock();

        var friend = Meteor.users.findOne(friend_id);

        if (!Meteor.user()) {
            throw new Meteor.Error(403, 'You need to be logged in, to send message');
        }
        if (!friend) {
            throw new Meteor.Error(404, 'No such friend');
        }

        var friends_requests = friend.friends_requests;

        if (_.indexOf(friends_requests, Meteor.userId()) === -1) {
            return false;
        }

        friends_requests = _.without(friends_requests, Meteor.userId());

        return Meteor.users.update(friend_id, {$set: {friends_requests: friends_requests}});
    },
    'friends/removeFriend': function(friend_id) {
        this.unblock();

        var friend = Meteor.users.findOne(friend_id);
        var user = Meteor.user();

        if (!Meteor.user()) {
            throw new Meteor.Error(403, 'You need to be logged in, to send message');
        }
        if (!friend) {
            throw new Meteor.Error(404, 'No such friend');
        }

        var friends1 = user.friends;
        var friends2 = friend.friends;

        if (_.indexOf(friends1, friend._id) !== -1) {
            friends1 = _.without(friends1, friend._id);
        }
        if (_.indexOf(friends2, user._id) !== -1) {
            friends2 = _.without(friends2, user._id);
        }

        Meteor.users.update(user._id, {$set: {friends: friends1}});
        Meteor.users.update(friend._id, {$set: {friends: friends2}});

        return true;
    },
    'friends/rejectRequest': function(friend_id) {
        this.unblock();

        if (!Meteor.user()) {
            throw new Meteor.Error(403, 'You need to be logged in, to send message');
        }
        if (!friend_id) {
            throw new Meteor.Error(404, 'No such friend');
        }

        var friends_requests = Meteor.user().friends_requests;

        if (_.indexOf(friends_requests, friend_id) === -1) {
            return false;
        }

        friends_requests = _.without(friends_requests, friend_id);

        return Meteor.users.update(Meteor.userId(), {$set: {friends_requests: friends_requests}});
    },
    'friends/acceptRequest': function(friend_id) {
        this.unblock();

        var friend = Meteor.users.findOne(friend_id);
        var user = Meteor.user();

        if (!user) {
            throw new Meteor.Error(403, 'You need to be logged in, to send message');
        }
        if (!friend) {
            throw new Meteor.Error(404, 'No such friend');
        }

        var friends_requests = user.friends_requests;
        var friends1 = user.friends;
        var friends2 = friend.friends;

        if (!_.isArray(friends1)) {
            friends1 = [];
        }

        if (!_.isArray(friends2)) {
            friends2 = [];
        }

        if (_.indexOf(friends_requests, friend._id) !== -1) {
            friends_requests = _.without(friends_requests, friend_id);
        }
        if (_.indexOf(friends1, friend._id) === -1) {
            friends1.push(friend._id);
        }
        if (_.indexOf(friends2, user._id) === -1) {
            friends2.push(user._id);
        }

        Meteor.users.update(user._id, {$set: {friends: friends1, friends_requests: friends_requests}});
        Meteor.users.update(friend_id, {$set: {friends: friends2}});

        return true;
    }
});
